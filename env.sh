#install conda via miniconda :
follow this tuto : https://docs.conda.io/en/latest/miniconda.html#linux-installers

#create your conda env
conda create -n snakemake 
conda activate snakemake
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda install -c anaconda python
conda install -c bioconda snakemake 
conda install -c anaconda pandas
conda install -c bioconda pysam
conda install -c conda-forge singularity
