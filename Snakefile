#########################################################################
# ePeak: Standardize and reproducible ChIP-seq analysis from raw        #
#           data to differential analysis                               #
# Authors: Rachel Legendre, Maelle Daunesse                             #
# Copyright (c) 2019-2020  Institut Pasteur (Paris) and CNRS.           #
#                                                                       #
# This file is part of ePeak workflow.                                  #
#                                                                       #
# ePeak is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# ePeak is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with ePeak (LICENSE).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################




import pandas as pd
from fnmatch import fnmatch
from re import sub, match
from itertools import repeat, chain
import os
import glob

#-------------------------------------------------------
# read config files

configfile: "config/config.yaml"
#put a relative path
RULES = os.path.join("workflow", "rules")

design_user = pd.read_csv(config["design"]["design_file"], header=0, sep='\t')

"""
REQUIREMENTS IN DESIGN:
 - all files in one directory
 - fullname of files must be :
        MARK_COND_REP_MATE.fastq.gz
 - name on design must be:
        MARK_COND


"""

#-------------------------------------------------------
# list of all files in the directory 'input_dir'

filenames = [f for f in os.listdir(config["input_dir"]) if match(r'.*'+config["input_mate"]+config["input_extension"]+'', f)] 
if not filenames :
    raise ValueError("Please provides input fastq files")

#-------------------------------------------------------
# paired-end data gestion

mate_pair = config["input_mate"]
rt1 = mate_pair.replace("[12]", "1")
rt2 = mate_pair.replace("[12]", "2")

R1 = [1 for this in filenames if rt1 in this]
R2 = [1 for this in filenames if rt2 in this]

if len(R2) == 0:
    paired = False
else:
    if R1 == R2:
        paired = True
    else:
        raise ValueError("Please provides single or paired files only")

#get sample names
filename_R1 = [ file for file in filenames if match(r'.*'+rt1+config["input_extension"]+'', file)]
samples = [sub(rt1+config["input_extension"], '', file) for file in filename_R1]
marks = [ x.strip() for x in (config["design"]["marks"]).split(",")]
conds = [ x.strip() for x in (config["design"]["condition"]).split(",")]
rep_flag = config["design"]["replicates"]


# -----------------------------------------------
# Compute number of replicates by IP/INPUT

#get uniq IP and INPUT
unique_ip =  pd.unique(design_user["IP_NAME"])
unique_input =  pd.unique(design_user["INPUT_NAME"])
#Fill new design with informations about IP
design = pd.DataFrame(unique_ip, columns = ['IP_NAME'])
design['NB_IP'] = design_user["IP_NAME"].value_counts().values
#initialize INPUT related columns
inputs = []
nb_input = []
#fill new design with informations about INPUT
for row in design.itertuples(index=True, name='Pandas'):
    inputs.append(design_user.loc[design_user['IP_NAME'] == getattr(row, "IP_NAME"), 'INPUT_NAME'].iloc[0])
    nb_input.append(design_user.loc[design_user['IP_NAME'] == getattr(row, "IP_NAME"), 'NB_INPUT'].iloc[0])
design['INPUT_NAME'] = inputs
design['NB_INPUT'] = nb_input


# -----------------------------------------------
# get list of INPUT files

INPUT = []
for row in design.itertuples(index=True, name='Pandas'):
    for file in samples:
        mark = getattr(row, "INPUT_NAME")
        if not pd.isna(mark):
            if fnmatch(file, mark+"*"+rep_flag+"1*"):
                i = 1
                name = sub(mate_pair, '', file)
                INPUT.append(name)
                if getattr(row, "NB_IP") > 1 :
                    while i < getattr(row, "NB_IP"):
                        INPUT.append(name)
                        i += 1

# -----------------------------------------------
# get list of IP files

IP_ALL = []
for mark in design['IP_NAME']:
    for file in samples:
        if fnmatch(file, mark+"*") and file not in IP_ALL:
            name = sub(mate_pair, '', file)
            IP_ALL.append(name)


# -----------------------------------------------
# check design
# Select mark for statistical analysis if there is more than one condition (with min 2 rep) for each mark


MARK_OK = []
CORR_INPUT_OK = [] # corresponding INPUT condition
nb = 0
for row in design.itertuples(index=True, name='Pandas'):
    for mark in marks:
        if mark == getattr(row, "IP_NAME").split("_")[0]:
            nb_rep = getattr(row, "NB_IP")
            if (getattr(row, "IP_NAME")).endswith(tuple(conds)) and getattr(row, "NB_IP") > 1:
                if nb >= 1 and mark not in MARK_OK:               
                    MARK_OK.append(mark)
                    CORR_INPUT_OK.append(getattr(row, "INPUT_NAME").split("_")[0])
                    break
                else:                 
                    nb += 1
nb = 0


if config["differential_analysis"]["input_counting"]:
    CORR_INPUT = [] # corresponding INPUT files for input counting
    for mark in CORR_INPUT_OK:
        for file in samples:
            if fnmatch(file, mark+"_*") and file not in CORR_INPUT:
                name = sub(mate_pair, '', file)
                CORR_INPUT.append(name)
    
  

# -----------------------------------------------
# built list of MARK_COND_REP from config.yaml and check correspondance between design, config and fastq files

if len(conds) > 1 :
#built list of MARK_COND_REP from config.yaml
    conf_cond = ["{mark}_{cond}_{flag}".format(cond=cond, mark=mark, flag=rep_flag) for cond in conds for mark in marks]
    for row in design.itertuples(index=True, name='Pandas'):
        for elem in conf_cond:
            if elem in getattr(row, "IP_NAME"):
                raise ValueError("Please check correspondance between config and design file: %s is not %s "
                                 % (elem,getattr(row, "IP_NAME")))
            elif not any(elem in s for s in samples):
                raise ValueError("Please check correspondance between config file and fastq filenames: %s not found in %s"
                                 % (elem, samples))
            elif sum(getattr(row, "IP_NAME")+"_"+rep_flag in s for s in samples) is not int(getattr(row, "NB_IP")):
                raise ValueError("Please check correspondance between number of replicates and/or prefix names in design "
                                 "file and fastq filenames: %s not found %s times in %s" % (getattr(row, "IP_NAME"),
                                                                                             getattr(row, "NB_IP"),samples))
            elif sum(getattr(row, "INPUT_NAME")+"_"+rep_flag in s for s in samples) is not int(getattr(row, "NB_INPUT")):
                raise ValueError("Please check correspondance between number of replicates and/or prefix names in design "
                                 "file and fastq filenames: %s not found %s times in %s" % (getattr(row, "INPUT_NAME"), getattr(row, "NB_INPUT"),samples))


# -----------------------------------------------
# get correspondance between IP and INPUT files
d = {}
d["IP"] = []
d["INPUT"] = []
for row in design_user.itertuples(index=True, name='Pandas'):     
        d["IP"].append(getattr(row, "IP_NAME") +"_"+ rep_flag + str(getattr(row, "NB_IP")))
        d["INPUT"].append(getattr(row, "INPUT_NAME") +"_"+ rep_flag + str(getattr(row, "NB_INPUT")))

IP_INPUT = pd.DataFrame.from_dict(d)

# -----------------------------------------------
# get REP names

###### INCLUDE BETTER GESTION OF AUTO REPLICATES WITH 3 REP
###### TODO use https://snakemake.readthedocs.io/en/stable/project_info/faq.html#i-don-t-want-expand-to-use-every-wildcard-what-can-i-do 
#get list with replicates names for all IP

if nb_rep > 1 :
    rep = [rep_flag+'1', rep_flag+'2']
    # deduce from rep the list for SPR & PPR
    ppr = ['PPR1', 'PPR2', 'PPRPool']
    spr = ['SPR1.1', 'SPR1.2', 'SPR2.1', 'SPR2.2']
else :
    rep = [rep_flag+'1']


# -----------------------------------------------
# From the design file, we get only IP with more than one replicate and at least one INPUT


IP_REP = [] # all IP passing IDR
IP_REP_DUP = [] # corresponding INPUT
IP_NO_INPUT = [] # all IP with replicates but without INPUT
INPUT_NA = [] # corresponding INPUT (list of NA according to number of IP)
IP_NA = [] # all IP without replicates and without INPUT
for row in design.itertuples(index=True, name='Pandas'):
    if getattr(row, "NB_IP") > 1 and getattr(row, "NB_INPUT") > 1:
        IP_REP_DUP.append(getattr(row, "INPUT_NAME")+"_Pool")
        IP_REP.append(getattr(row, "IP_NAME"))
    elif getattr(row, "NB_IP") > 1 and getattr(row, "NB_INPUT") == 1:
        IP_REP_DUP.append(getattr(row, "INPUT_NAME")+"_"+rep_flag+"1")
        IP_REP.append(getattr(row, "IP_NAME"))
    elif getattr(row, "NB_IP") > 1 and getattr(row, "NB_INPUT") < 1:
        IP_NO_INPUT.append(getattr(row, "IP_NAME"))
        INPUT_NA.append('NA')
    elif getattr(row, "NB_IP") == 1 and getattr(row, "NB_INPUT") < 1:
        IP_NA.append(getattr(row, "IP_NAME"))

# We get only INPUT with at least one replicate that are linked to an IP with multiple replicates
INPUT_REP = []
for row in design.itertuples(index=True, name='Pandas'):
    if getattr(row, "NB_INPUT") > 1 and getattr(row, "NB_IP") > 1 and (getattr(row, "INPUT_NAME")) not in INPUT_REP:
        INPUT_REP.append(getattr(row, "INPUT_NAME"))


# -----------------------------------------------
# get IP passed pre IDR step (for rep, ppr and spr)

IP_IDR = []
IP_SPR = []
IP_PPR = []
PPR_POOL = []
INPUT_SPR  = []
INPUT_PPR = []
#IDR_REP = []
# for only IP with more than one replicate
for cond in IP_REP:
    tmp = []
    for ip in IP_ALL:
        name = ip.split("_" + rep_flag)
        if ip.startswith(cond):
            spr_file = sub(rep_flag, 'SPR', ip)
            ppr_file =  sub(rep_flag, 'PPR', ip)
            pool_file = sub(r''+rep_flag+'[0-9]*', 'PPRPool', ip)
            tmp.append(ip)
            #IDR_REP.append("Rep"+name[1])
            IP_SPR.append(spr_file+".1")
            IP_SPR.append(spr_file+".2")
            IP_PPR.append(ppr_file)
            PPR_POOL.append(pool_file)
            # add corresponding INPUT
            #get INPUT for this IP and this rep
            ctl = design_user.loc[(design_user['IP_NAME'] == name[0]) & (design_user['NB_IP'] == int(name[1])), 'INPUT_NAME'].iloc[0]+"_"+rep_flag+str(design_user.loc[(design_user['IP_NAME'] == name[0]) & (design_user['NB_IP'] == int(name[1])), 'NB_INPUT'].iloc[0])
            #check is INPUT is a POOL or not
            if design.loc[design['INPUT_NAME'] == (design_user.loc[design_user['IP_NAME'] == name[0], 'INPUT_NAME'].iloc[0]), 'NB_INPUT'].iloc[0] > 1 :
                ctl_pool = (design_user.loc[design_user['IP_NAME'] == name[0], 'INPUT_NAME'].iloc[0])+'_Pool'
            else: ctl_pool = ctl
            #tmp_df from all preIDR + input
            tmp_df = pd.DataFrame({"IP":[spr_file+".1",spr_file+".2", ppr_file, pool_file],"INPUT":[ctl, ctl, ctl, ctl_pool]})
            #add to IP_INPUT
            IP_INPUT = IP_INPUT.append(tmp_df, ignore_index = True)
    IP_IDR.append(tmp)


# -----------------------------------------------
# get pooled IP passed pre IDR step and corresponding INPUT

PPR_POOL = []
INPUT_POOL = []
for row in design.itertuples(index=True, name='Pandas'):
    #print(row)
    if getattr(row, "NB_IP") > 1 and getattr(row, "NB_INPUT") > 1 :
        PPR_POOL.append(getattr(row, "IP_NAME") + "_PPRPool")
        INPUT_POOL.append(getattr(row, "INPUT_NAME") + "_Pool")
        pass_pool = 0
    elif getattr(row, "NB_IP") > 1 and getattr(row, "NB_INPUT") == 1 :
        PPR_POOL.append(getattr(row, "IP_NAME") + "_PPRPool")
        INPUT_POOL.append(getattr(row, "INPUT_NAME")+ "_" + rep_flag + "1")
        pass_pool = 1



# all files passing PhantomPeakQualTool if no-model is chosen
ALL = IP_ALL + IP_SPR + IP_PPR + PPR_POOL


# all files passing peak calling step
ALL_IP_PC = IP_ALL + IP_SPR + IP_PPR + PPR_POOL
ALL_INPUT_PC = INPUT + INPUT_SPR + INPUT_PPR + INPUT_POOL

# get files for IDR
CASE = [rep_flag, "PPR", "SPR1.", "SPR2."]*len(IP_REP)
REP_IDR = list(chain(*zip(*repeat(IP_REP,4))))
IN_IDR = list(chain(*zip(*repeat(IP_REP_DUP,4))))

# -----------------------------------------------
# Add wildcard constraints

wildcard_constraints:
    sample = "[A-Za-z-_0-9]+_{0}[0-9]+".format(rep_flag),
    IP_REP = "[A-Za-z-_0-9]+_{0}[0-9]+".format(rep_flag),
    REP = "{0}[0-9]+".format(rep_flag),
    SPR = "[A-Za-z-_0-9]+_SPR[0-9]\.[1-4]*",
    PPR = "[A-Za-z-_0-9]+_PPR[0-9]*",
    POOL = "[A-Za-z-_0-9]+_PPRPool",
    INPUT_POOL = "[A-Za-z-_0-9]+_(Pool|{0}1)".format(rep_flag),
    MARK = "[A-Za-z-_0-9]+"

# -----------------------------------------------
#initialize global variables

analysis_dir = config["analysis_dir"]
if not os.path.exists(analysis_dir):
    os.makedirs(analysis_dir)

sample_dir = config["input_dir"]

input_data = [sample_dir + "/{{SAMPLE}}{}{}".format(rt1,config["input_extension"])]
if paired:
    input_data += [sample_dir + "/{{SAMPLE}}{}{}".format(rt2,config["input_extension"])]    
# global variable for all output files
final_output = []

# -----------------------------------------------
#begin of the rules

#----------------------------------
# quality control
#----------------------------------

fastqc_input_fastq = input_data
fastqc_output_done = os.path.join(analysis_dir, "00-Fastqc/{{SAMPLE}}{}_fastqc.done".format(rt1))
fastqc_wkdir = os.path.join(analysis_dir, "00-Fastqc")
fastqc_log = os.path.join(analysis_dir, "00-Fastqc/logs/{{SAMPLE}}{}_fastqc_raw.log".format(rt1))
final_output.extend(expand(fastqc_output_done, SAMPLE=samples))
include: os.path.join(RULES, "fastqc.rules")



#----------------------------------
# Remove adapters
#----------------------------------

if config["adapters"]["remove"] :

    ## TODO add AlienTrimmer
    adapter_tool = "adapters"
    cutadapt_input_fastq = input_data
    cutadapt_wkdir = os.path.join(analysis_dir, "01-Trimming")
    cutadapt_output = [os.path.join(analysis_dir, "01-Trimming/{{SAMPLE}}{}_trim{}".format(rt1,config["input_extension"]))]
    if paired:
        cutadapt_output += [os.path.join(analysis_dir, "01-Trimming/{{SAMPLE}}{}_trim{}".format(rt2,config["input_extension"]))]

    # Set parameters
    #todo write the good line according to tool
    cutadapt_adapt_list = config["adapters"]["adapter_list"]
    
    cutadapt_options = config["adapters"]["options"]
    cutadapt_mode = config["adapters"]["mode"]
    cutadapt_min  = config["adapters"]["m"]
    cutadapt_qual = config["adapters"]["quality"]
    cutadapt_log = os.path.join(analysis_dir, "01-Trimming/logs/{SAMPLE}_trim.txt")
    #final_output.extend(expand(cutadapt_output, SAMPLE=samples))
    include: os.path.join(RULES, "cutadapt.rules")

else:
    cutadapt_output = input_data


#----------------------------------
# genome gestion
#----------------------------------

ref = config["genome"]["name"]

#if config["design"]["spike"]:
#    ref += [ "spikes" ]
    # TODO add possibility of index name and also, check if 


def mapping_index(wildcards):
    if (wildcards.REF == config["genome"]["name"]):
        input = config["genome"]["fasta_file"]
    elif (wildcards.REF == "spikes"):
        input = config["design"]["spike_genome_file"]
    return(input)

#----------------------------------
# bowtie2 indexing
#----------------------------------

if config["genome"]["index"]: 
    # indexing for bowtie2
    bowtie2_index_fasta = mapping_index
    bowtie2_index_output_done = os.path.join(config["genome"]["genome_directory"],"{REF}.1.bt2")
    bowtie2_index_output_prefix = os.path.join(config["genome"]["genome_directory"],"{REF}")
    bowtie2_index_log = os.path.join(analysis_dir, "02-Mapping/logs/bowtie2_{REF}_indexing.log")
    #final_output.extend(expand(bowtie2_index_output_done, REF=ref))
    include: os.path.join(RULES, "bowtie2_index.rules")

else:
    bowtie2_index_output_done = os.path.join(config["genome"]["genome_directory"],"{REF}.1.bt2")
    bowtie2_index_output_prefix = os.path.join(config["genome"]["genome_directory"],"{REF}")

#----------------------------------
# bowtie2 MAPPING
#----------------------------------

bowtie2_mapping_input = cutadapt_output
bowtie2_mapping_index_done = bowtie2_index_output_done
bowtie2_mapping_sort = os.path.join(analysis_dir, "02-Mapping/{SAMPLE}_{REF}_sort.bam")
bowtie2_mapping_bam = os.path.join(analysis_dir, "02-Mapping/{SAMPLE}_{REF}.bam")
bowtie2_mapping_sortprefix = os.path.join(analysis_dir, "02-Mapping/{SAMPLE}_{REF}_sort")
bowtie2_mapping_logs_err = os.path.join(analysis_dir, "02-Mapping/logs/{SAMPLE}_{REF}_mapping.err")
bowtie2_mapping_logs_out = os.path.join(analysis_dir, "02-Mapping/logs/{SAMPLE}_{REF}_mapping.out")
bowtie2_mapping_prefix_index = bowtie2_index_output_prefix
bowtie2_mapping_options =  config["bowtie2_mapping"]["options"]
#final_output.extend(expand(bowtie2_mapping_sort, SAMPLE=samples, REF=ref))
include: os.path.join(RULES, "bowtie2_mapping.rules")
biasedRegions_dir = os.path.join(analysis_dir, "02-Mapping")
biasedRegions = ""


#----------------------------------
# Mark duplicated reads
#----------------------------------
dedup_IP = config['mark_duplicates']['dedup_IP']

def dedup(wildcards):
    if dedup_IP == "true":
        return "true" 
    elif (wildcards.SAMPLE in IP_ALL) and dedup_IP == "false":
        return "false"
    else :
        return "true"

if config['mark_duplicates']['do']:
    biasedRegions = "_dedup"
    biasedRegions_dir = os.path.join(analysis_dir, "03-Deduplication")
    mark_duplicates_input = bowtie2_mapping_sort
    mark_duplicates_output = os.path.join(analysis_dir, "03-Deduplication/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions))
    mark_duplicates_metrics = os.path.join(analysis_dir, "03-Deduplication/{SAMPLE}_{REF}_sort_dedup.txt")
    mark_duplicates_remove = dedup
    mark_duplicates_log_std = os.path.join(analysis_dir, "03-Deduplication/logs/{SAMPLE}_{REF}_sort_dedup.out")
    mark_duplicates_log_err = os.path.join(analysis_dir, "03-Deduplication/logs/{SAMPLE}_{REF}_sort_dedup.err")
    mark_duplicates_tmpdir = config['tmpdir']
    #final_output.extend(expand(mark_duplicates_output, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "mark_duplicates.rules")



#----------------------------------
# Spikes counting
#----------------------------------

if config["design"]["spike"]:
    # counting on spikes
    spikes_counting_input = expand(os.path.join(analysis_dir, "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions)), SAMPLE=samples, REF="spikes")
    spikes_counting_output_json = os.path.join(analysis_dir, "09-CountMatrix/Spikes_count.json")
    spikes_counting_output = os.path.join(analysis_dir, "Spikes_metrics.out")
    spikes_counting_log = os.path.join(analysis_dir, "03-Deduplication/logs/Spikes_metrics.out")
    final_output.extend([spikes_counting_output_json])
    include: os.path.join(RULES, "spikes_counting.rules")

   
    #compute scale factor if spike-in 
    compute_scaling_factor_input = "{}/{{SAMPLE}}_spikes_sort{}.bam".format(biasedRegions_dir, biasedRegions)
    compute_scaling_factor_output = "{}/{{SAMPLE}}_scaleFactor.txt".format(biasedRegions_dir, biasedRegions)
    #final_output.extend(expand(compute_scaling_factor_output, SAMPLE=samples))
    include: os.path.join(RULES, "compute_scaling_factor.rules")

#----------------------------------
# Remove biased regions
#----------------------------------

if config["remove_biasedRegions"]["do"]:
    remove_biasedRegions_input = os.path.join(analysis_dir, "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions))
    biasedRegions += "_biasedRegions"
    biasedRegions_dir = os.path.join(analysis_dir, "04-NobiasedRegions")
    remove_biasedRegions_output = os.path.join(analysis_dir, "04-NobiasedRegions/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions))
    remove_biasedRegions_log_std = os.path.join(analysis_dir, "04-NobiasedRegions/logs/{{SAMPLE}}_{{REF}}_sort{}.out".format(biasedRegions))
    remove_biasedRegions_log_err = os.path.join(analysis_dir, "04-NobiasedRegions/logs/{{SAMPLE}}_{{REF}}_sort{}.err".format(biasedRegions))
    #final_output.extend(expand(remove_biasedRegions_output, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "remove_biasedRegions.rules")

#----------------------------------
# Coverage step
#----------------------------------
if config["bamCoverage"]["do"]:
    bamCoverage_input = "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions)
    bamCoverage_logs = os.path.join(analysis_dir, "12-IGV/logs/{SAMPLE}_{REF}.out")
    bamCoverage_output = os.path.join(analysis_dir, "12-IGV/{SAMPLE}_{REF}_coverage.bw")
    bamCoverage_options = config['bamCoverage']['options']
    if config["bamCoverage"]["spike-in"] and config["design"]["spike"]:
        bamCoverage_scaleFactor = compute_scaling_factor_output
    else:
        bamCoverage_scaleFactor = "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions)
    final_output.extend(expand(bamCoverage_output,  SAMPLE=samples, REF=ref, allow_missing=True))    
    include: os.path.join(RULES, "bamCoverage.rules")  


#----------------------------------
# Estimate Library Complexity
#----------------------------------
if paired:
    lib_complexity_input = "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions)
    lib_complexity_metrics = os.path.join(analysis_dir, "05-QC/Complexity/{SAMPLE}_{REF}_metrics.txt")
    lib_complexity_log_std = os.path.join(analysis_dir, "05-QC/Complexity/logs/{SAMPLE}_{REF}_complexity.out")
    lib_complexity_log_err = os.path.join(analysis_dir, "05-QC/Complexity/logs/{SAMPLE}_{REF}_complexity.err")
    final_output.extend(expand(lib_complexity_metrics,  SAMPLE=samples, REF=ref))    
    include: os.path.join(RULES, "EstimateLibraryComplexity.rules")

#----------------------------------
# plot Fingerprint
#----------------------------------
def IPandINPUT(wildcards):
    return [str(biasedRegions_dir + "/" + IP_INPUT[IP_INPUT.IP == wildcards.IP].iloc[0]['INPUT'] + "_{}_sort{}.bam".format(ref, biasedRegions)), "{}/{{IP}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)]


plotFingerprint_input = IPandINPUT
plotFingerprint_output = os.path.join(analysis_dir, "05-QC/Fingerprint/{{IP}}_{}_fingerprint.pdf".format(ref))
plotFingerprint_output_raw =  os.path.join(analysis_dir, "05-QC/Fingerprint/{{IP}}_{}_fingerprint_rawcounts.txt".format(ref))
plotFingerprint_options = ""
plotFingerprint_logs = os.path.join(analysis_dir, "05-QC/Fingerprint/logs/{{IP}}_{}_Fingerprint.out".format(ref))
final_output.extend(expand(plotFingerprint_output,  IP=IP_ALL))    
include: os.path.join(RULES, "plotFingerprint.rules")

#----------------------------------
# GeneBody plot
#----------------------------------
if config['geneBody']['do']:
    computeMatrix_input = bamCoverage_output
    computeMatrix_output = os.path.join(analysis_dir, "05-QC/GeneBody/{SAMPLE}_{REF}.mat.gz")
    computeMatrix_regions = config['geneBody']['regionsFileName']
    computeMatrix_options = " --beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros "
    computeMatrix_mode = "scale-regions"
    computeMatrix_logs = os.path.join(analysis_dir, "05-QC/GeneBody/logs/{SAMPLE}_{REF}_matrix.out")
    #final_output.extend(expand(computeMatrix_output,  SAMPLE=samples, REF=ref))    
    include: os.path.join(RULES, "computeMatrix.rules")

    plotHeatmap_input = computeMatrix_output
    plotHeatmap_output = os.path.join(analysis_dir, "05-QC/GeneBody/{SAMPLE}_{REF}_GeneBody.pdf")
    plotHeatmap_options = ""
    plotHeatmap_logs = os.path.join(analysis_dir, "05-QC/GeneBody/logs/{SAMPLE}_{REF}_GeneBody.out")
    final_output.extend(expand(plotHeatmap_output,  SAMPLE=samples, REF=ref))    
    include: os.path.join(RULES, "plotHeatmap.rules")


#------------------------------------------------
# fragment size distribution - Picard CollectInsertSizeMetrics
#------------------------------------------------
if paired:
    insert_size_metrics_input = "{}/{{SAMPLE}}_{{REF}}_sort{}.bam".format(biasedRegions_dir, biasedRegions)
    insert_size_metrics_output_txt = os.path.join(analysis_dir, "05-QC/FragmentSizeDistribution/{SAMPLE}_{REF}_fragmentSizeDistribution.txt")
    insert_size_metrics_histo = os.path.join(analysis_dir, "05-QC/FragmentSizeDistribution/{SAMPLE}_{REF}_fragmentSizeDistribution.pdf")
    insert_size_metrics_log_out = os.path.join(analysis_dir, "05-QC/FragmentSizeDistribution/logs/{SAMPLE}_{REF}_fragment_size.out")
    insert_size_metrics_log_err = os.path.join(analysis_dir, "05-QC/FragmentSizeDistribution/logs/{SAMPLE}_{REF}_fragment_size.err")
    final_output.extend(expand(insert_size_metrics_output_txt, SAMPLE=samples, REF=ref))
    include: os.path.join(RULES, "fragment_size_distribution.rules")

#-------------------------------------
# then we performed peak calling only in files against reference genome and not spike genome !
ref = config["genome"]["name"] 

#----------------------------------
# preIDR on INPUT
#----------------------------------

if len(INPUT_REP) > 0 and pass_pool == 0 :
    preIDR_pool_input_bam = expand("%s/{{INPUT}}_{REP}_%s_sort%s.bam" % (biasedRegions_dir,ref, biasedRegions), REP = rep)
    preIDR_pool_log = "%s/logs/{INPUT}_preIDR_input.o" % (biasedRegions_dir)
    if len(rep) > 2:
        preIDR_pool_output = ["{}/{{INPUT}}_Pool_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions),
                                 "{}/{{INPUT}}_MaxiPool_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)]
    else:
        preIDR_pool_output = "{}/{{INPUT}}_Pool_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)

    #final_output.extend(expand(preIDR_pool_output, INPUT=INPUT_REP))
    include: os.path.join(RULES, "preIDR_Pool.rules")

#----------------------------------
# preIDR on IP
#----------------------------------

if len(IP_REP) > 0:
    # run SPR
    preIDR_SPR_input_bam = expand("%s/{{IP}}_{REP}_%s_sort%s.bam" % (biasedRegions_dir,ref, biasedRegions) , REP = rep)
    preIDR_SPR_log = "%s/logs/{IP}_preIDR_SPR.o"% (biasedRegions_dir)
    preIDR_SPR_output = expand("%s/{{IP}}_{SPR}_%s_sort%s.bam" % (biasedRegions_dir,ref, biasedRegions),  SPR = spr)
    #final_output.extend(expand(preIDR_SPR_output, IP=IP_REP))
    include: os.path.join(RULES, "preIDR_SPR.rules")

    # run PPR
    preIDR_PPR_input_bam = expand("%s/{{IP}}_{REP}_%s_sort%s.bam" % (biasedRegions_dir,ref, biasedRegions), REP = rep)
    preIDR_PPR_log = "%s/logs/{IP}_preIDR_PPR.o" % (biasedRegions_dir)
    preIDR_PPR_output =  expand("%s/{{IP}}_{PPR}_%s_sort%s.bam" % (biasedRegions_dir,ref, biasedRegions), PPR = ppr)
    #final_output.extend(expand(preIDR_PPR_output, IP=IP_REP))
    include: os.path.join(RULES, "preIDR_PPR.rules")

#----------------------------------
# Cross correlation
#----------------------------------

if config["macs2"]["no_model"]:
    # PhantomPeakQualTools rule
    spp_input = "{}/{{ALL}}_{}_sort{}.bam".format(biasedRegions_dir,ref, biasedRegions)
    spp_output_pdf = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/{{ALL}}_{}_sort{}_phantom.pdf".format(ref, biasedRegions))
    spp_metrics = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/{{ALL}}_{}_sort{}_spp.out".format(ref, biasedRegions))
    spp_log_std = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/logs/{ALL}_phantom.out")
    spp_log_err = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/logs/{ALL}_phantom.err")
    spp_tmpdir = config['tmpdir']
    final_output.extend(expand(spp_metrics, ALL=ALL))
    include: os.path.join(RULES, "spp.rules")
else :
    # PhantomPeakQualTools rule
    spp_input = "{}/{{ALL}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)
    spp_output_pdf = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/{{ALL}}_{}_sort{}_phantom.pdf".format(ref, biasedRegions))
    spp_metrics = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/{{ALL}}_{}_sort{}_spp.out".format(ref, biasedRegions))
    spp_log_std = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/logs/{ALL}_phantom.out")
    spp_log_err = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/logs/{ALL}_phantom.err")
    spp_tmpdir = config['tmpdir']
    final_output.extend(expand(spp_metrics, ALL=IP_ALL))
    include: os.path.join(RULES, "spp.rules")


#----------------------------------
# Peak Calling with MACS2
#----------------------------------
if config["macs2"]["do"]:
    def INPUTtoIP(wildcards):
        return str(biasedRegions_dir + "/" + IP_INPUT[IP_INPUT.IP == wildcards.SAMPLE].iloc[0]['INPUT'] + "_{}_sort{}.bam".format(ref, biasedRegions))

    model = config["macs2"]["mode_choice"]
    model_dir = model
    mod = [model]
    peak_caller = ["macs2"]
    if config["macs2"]["no_model"]:
        model_dir += "-nomodel"
    if model in ["narrow", "broad"]:
        # Peak Calling on replicates
        if model in ["narrow"]:
            # add corresponding options
            macs2_options = "-p {} ".format(config["macs2"]['cutoff']) + config["macs2"]['options']
        else:
            macs2_options = "-p {} --broad --broad-cutoff {} ".format(config["macs2"]['cutoff'],
                                config["macs2"]['cutoff']) + config["macs2"]['options']
        if config["macs2"]["no_model"]:
            macs2_options += " --nomodel "
            macs2_shift_file = os.path.join(analysis_dir, "05-QC/PhantomPeakQualTools/{{SAMPLE}}_{}_sort{}_spp.out".format(ref, biasedRegions))
        else:
            macs2_shift_file = "{}/{{SAMPLE}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)
        if paired :
            macs2_pe_mode = "no"
        else:
            macs2_pe_mode = "no"

        macs2_input_bam = "{}/{{SAMPLE}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)
        macs2_control = INPUTtoIP
        macs2_log_out = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/logs/{{SAMPLE}}.out".format(model_dir))
        macs2_log_err = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/logs/{{SAMPLE}}.err".format(model_dir))
        macs2_output = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/{{SAMPLE}}_peaks.{}Peak".format(model_dir, model))
        macs2_output_prefix = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/{{SAMPLE}}".format(model_dir))

        final_output.extend(expand(macs2_output, zip, SAMPLE=ALL_IP_PC))
        include: os.path.join(RULES, "macs2.rules")
    else :
        raise ValueError("Please provides valid model for MACS2")


#----------------------------------
# Peak Calling with SEACR
#----------------------------------

if config["seacr"]["do"]:
    if not config["macs2"]["do"]:
        peak_caller = ["seacr"]
        mod = [config["seacr"]["threshold"]]
    else:
        peak_caller += ["seacr"]
        mod += [config["seacr"]["threshold"]]

    # produce bedgrah files
    bedgraph_input = "{}/{{SAMPLE}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions)
    if config["design"]["spike"]:
        bedgraph_scaleFactor = compute_scaling_factor_output
    else:
        bedgraph_scaleFactor = bedgraph_input
    bedgraph_genome = config["genome"]["fasta_file"]+".fai"
    if paired:
        bedgraph_options = " -bedpe "
    else:
        bedgraph_options = ""
    bedgraph_logs = os.path.join(analysis_dir, "06-PeakCalling/seacr/logs/{SAMPLE}_bedgraph.out")
    bedgraph_output = os.path.join(analysis_dir, "06-PeakCalling/seacr/{SAMPLE}_bedgraph.bg")
    #final_output.extend(expand(bedgraph_output, SAMPLE=samples))
    include: os.path.join(RULES, "bedgraph.rules")

    def IGGtoIP(wildcards):
        return str(os.path.join(analysis_dir,"06-PeakCalling/seacr/") + IP_INPUT[IP_INPUT.IP == wildcards.SAMPLE].iloc[0]['INPUT'] + "_bedgraph.bg")

    seacr_bed_ip = bedgraph_output
    seacr_bed_input = IGGtoIP
    seacr_bed_threshold = config["seacr"]["threshold"]
    seacr_bed_norm = config["seacr"]["norm"]
    seacr_output_prefix = os.path.join(analysis_dir, "06-PeakCalling/seacr/{SAMPLE}")
    seacr_output = os.path.join(analysis_dir, "06-PeakCalling/seacr/{{SAMPLE}}.{}.bed".format(config["seacr"]["threshold"]))
    seacr_logs_std = os.path.join(analysis_dir, "06-PeakCalling/seacr/logs/{SAMPLE}_seacr_calling.out")
    seacr_logs_err = os.path.join(analysis_dir, "06-PeakCalling/seacr/logs/{SAMPLE}_seacr_calling.err")
    final_output.extend(expand(seacr_output, SAMPLE=IP_ALL))
    include: os.path.join(RULES, "seacr.rules")   


#----------------------------------
# Peak Calling metrics
#----------------------------------

if config["macs2"]["do"] or config["seacr"]["do"] :
    def stats_pc_input(wildcards):
        if wildcards.CALLER == "macs2":
            return expand(os.path.join(analysis_dir, "06-PeakCalling/{{CALLER}}/%s/{IP_REP}_peaks.{{MOD}}Peak" % (model_dir)), IP_REP=ALL_IP_PC)
        elif wildcards.CALLER == "seacr":
            return expand(os.path.join(analysis_dir, "06-PeakCalling/{{CALLER}}/{IP_REP}.{{MOD}}.bed"), IP_REP=IP_ALL)

    stats_peakCalling_input = stats_pc_input
    stats_peakCalling_csv = os.path.join(analysis_dir, "{CALLER}_{MOD}_Peaks_metrics_mqc.out")
    stats_peakCalling_marks = marks
    stats_peakCalling_conds = conds
    stats_peakCalling_rep = rep_flag
    stats_peakCalling_log = os.path.join(analysis_dir, "06-PeakCalling/{CALLER}/{MOD}_Peaks_metrics.out")
    include: os.path.join(RULES, "stats_peakCalling.rules")
    final_output.extend(expand(stats_peakCalling_csv, zip, CALLER=peak_caller, MOD=mod))


#----------------------------------
# IDR computing
#----------------------------------
if config["macs2"]["do"] and config["compute_idr"]["do"]:

    if model in ["narrow"]:
        compute_idr_mode = "narrowPeak"
    else :
        compute_idr_mode = "broadPeak"
    compute_idr_input1 = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/{{IP_IDR}}_{{CASE}}1_peaks.{}Peak".format(model_dir, model))
    compute_idr_input2 = os.path.join(analysis_dir, "06-PeakCalling/macs2/{}/{{IP_IDR}}_{{CASE}}2_peaks.{}Peak".format(model_dir, model))
    compute_idr_output = os.path.join(analysis_dir, "07-IDR/macs2/{}/{{IP_IDR}}_{{CASE}}1vs{{CASE}}2_{}_{}_idr.txt".format(model_dir, ref, model))
    compute_idr_output_peak = os.path.join(analysis_dir, "07-IDR/macs2/{}/{{IP_IDR}}_{{CASE}}1vs{{CASE}}2_{}_{}_idr{}.{}Peak".format(model_dir, 
        ref, model, config["compute_idr"]["thresh"], model))
    compute_idr_log = os.path.join(analysis_dir, "07-IDR/macs2/{}/logs/{{IP_IDR}}_{{CASE}}1vs{{CASE}}2_{}_idr.out".format(model_dir,model))
    include: os.path.join(RULES, "compute_idr.rules")
    final_output.extend(expand(compute_idr_output, zip, IP_IDR=REP_IDR, CASE=CASE))



#----------------------------------
# Select reproducible peaks
#----------------------------------
CALL_MOD = []

if config["macs2"]["do"] and model in ["narrow"] and not config["intersectionApproach"]["do"] and config["compute_idr"]["do"]:
    CALL_MOD += ["macs2_" + model_dir]
    # Select IDR peaks
    def IDR_input_rep(wildcards):
        #if wildcards.CALLER == "macs2_narrow":
        return str(os.path.join(analysis_dir, "07-IDR/macs2/"+model_dir+"/"+wildcards.IP_IDR+"_"+rep_flag+"1vs"+rep_flag+"2_"+ref+"_"+model+"_idr"+str(config["compute_idr"]["thresh"])+"."+model+"Peak"))

    def IDR_input_ppr(wildcards):
        #if wildcards.CALLER == "macs2_narrow":
            #return [os.path.join(analysis_dir, "07-IDR/macs2/%s/{IP_IDR}_%s1vs%s2_%s_%s_idr%s.%sPeak" % (model_dir, 
            #    "PPR", "PPR", ref, model, config["compute_idr"]["thresh"], model))]
        return str(os.path.join(analysis_dir, "07-IDR/macs2/"+model_dir+"/"+wildcards.IP_IDR+"_PPR1vsPPR2_"+ref+"_"+model+"_idr"+str(config["compute_idr"]["thresh"])+"."+model+"Peak"))

    def IDR_input_pool(wildcards):
        #if wildcards.CALLER == "macs2_narrow":
            #return [os.path.join(analysis_dir, "06-PeakCalling/macs2/%s/{IP_IDR}_PPRPool_peaks.%sPeak" % (model_dir, model))]
        return str(os.path.join(analysis_dir, "06-PeakCalling/macs2/"+model_dir+"/"+wildcards.IP_IDR+"_PPRPool_peaks."+model+"Peak"))

    select_peaks_input_rep = IDR_input_rep
    select_peaks_input_ppr = IDR_input_ppr
    select_peaks_input_pool = IDR_input_pool
    select_peaks_logs = os.path.join(analysis_dir, "08-ReproduciblePeaks/{{CALLER}}_{}/logs/{{IP_IDR}}_selected_peaks.o".format(model_dir))
    select_peaks_output = os.path.join(analysis_dir, "08-ReproduciblePeaks/{{CALLER}}_{}/{{IP_IDR}}_select.{}Peak".format(model_dir, model))
    include: os.path.join(RULES, "select_peaks.rules")
    final_output.extend(expand(select_peaks_output, CALLER="macs2", IP_IDR=IP_REP))

#----------------------------------
# Intersection approach
#----------------------------------

if config["seacr"]["do"] :
    CALL_MOD += ["seacr_" + config["seacr"]["threshold"]]
if config["macs2"]["do"] :
    CALL_MOD += ["macs2_" + model_dir]

def IA_input(wildcards):
    if wildcards.CALLER == "macs2_"+ model_dir:
        return expand(os.path.join(analysis_dir, "06-PeakCalling/{CALLER}/%s/{{IP_IDR}}_{CASE}_peaks.{MOD}Peak" % (model_dir)), CALLER="macs2", CASE=rep, MOD=config["macs2"]["mode_choice"])
    elif wildcards.CALLER == "seacr_"+ config["seacr"]["threshold"]:
        return expand(os.path.join(analysis_dir, "06-PeakCalling/{CALLER}/{{IP_IDR}}_{CASE}.{MOD}.bed"), CALLER="seacr", CASE=rep,  MOD=config["seacr"]["threshold"])

if (config["macs2"]["do"] and config["macs2"]["mode_choice"] in ["broad"]) or config["intersectionApproach"]["do"] or config["seacr"]["do"] :
    intersectionApproach_input_rep = IA_input
    intersectionApproach_logs = os.path.join(analysis_dir, "08-ReproduciblePeaks/{CALLER}/logs/{IP_IDR}_IA_peaks.o")
    intersectionApproach_output = os.path.join(analysis_dir, "08-ReproduciblePeaks/{CALLER}/{IP_IDR}_IA.bed")
    if config["macs2"]["do"] and config["macs2"]["mode_choice"] in ["broad"] :
        intersectionApproach_overlap = 0.8
    else:
        intersectionApproach_overlap = config["intersectionApproach"]["ia_overlap"]
    include: os.path.join(RULES, "intersectionApproach.rules")
    final_output.extend(expand(intersectionApproach_output, CALLER=CALL_MOD,  IP_IDR=IP_REP))
    

#----------------------------------
# Compute IA metrics
#----------------------------------

if (config["macs2"]["do"] and config["macs2"]["mode_choice"] in ["broad"]) or config["intersectionApproach"]["do"]:
    stats_IA_input = expand(intersectionApproach_output, CALLER=CALL_MOD,  IP_IDR=IP_REP)
    stats_IA_csv = os.path.join(analysis_dir, "IA_metrics.out")
    stats_IA_log = os.path.join(analysis_dir, "08-ReproduciblePeaks/{}/logs/IA_metrics.out".format(model_dir))
    include: os.path.join(RULES, "stats_IA.rules")
    final_output.extend([stats_IA_csv])

#----------------------------------
# Compute IDR metrics
#----------------------------------

if config["macs2"]["do"] and config["compute_idr"]["do"]:
    idr_peaks = []
    idr_peaks.extend(expand(os.path.join(analysis_dir, "07-IDR/macs2/{}/{{IP_IDR}}_{{CASE}}1vs{{CASE}}2_{}_{}_idr{}.{}Peak".format(model_dir,
                            ref,model, config["compute_idr"]["thresh"], model)), zip, IP_IDR=REP_IDR, CASE=CASE))
    metrics_peaks_input = idr_peaks
    metrics_peaks_marks = marks
    metrics_peaks_conds = conds
    metrics_peaks_rep = rep_flag
    metrics_peaks_logs = os.path.join(analysis_dir, "07-IDR/macs2/{}/logs/IDR_metrics.out".format(model_dir))
    metrics_peaks_output = os.path.join(analysis_dir, "IDR_metrics.out")
    include: os.path.join(RULES, "metrics_peaks.rules")
    final_output.extend([metrics_peaks_output])


#----------------------------------
# Run differential analysis
#----------------------------------

if len(conds) > 1 and config["differential_analysis"]["do"]:
    

    def getPeakFilesByMark(wildcards):
        if wildcards.CALLER in ["seacr_"+config["seacr"]["threshold"], "macs2_broad", "macs2_broad-nomodel" ] or (wildcards.CALLER in  ["macs2_narrow","macs2_narrow-nomodel"] and config["intersectionApproach"]["do"]): 
            return expand(os.path.join(analysis_dir, "08-ReproduciblePeaks/{CALLER}/{MARK}_{COND}_IA.bed"), 
                CALLER=wildcards.CALLER, MARK=wildcards.MARK, COND=conds)
        else :
            return expand(os.path.join(analysis_dir, "08-ReproduciblePeaks/{{CALLER}}/{{MARK}}_{{COND}}_select.{}Peak".format(model)), 
                CALLER=wildcards.CALLER, MARK=wildcards.MARK, COND=conds)
 


    #----------------------------------
    # get union peaks
    #----------------------------------

    union_peaks_input = getPeakFilesByMark
    union_peaks_logs = os.path.join(analysis_dir, "09-CountMatrix/{CALLER}/logs/{MARK}_Optimal_selected_peaks.out")
    if len(conds) == 2 :
        union_peaks_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}_{}.optimal.Peak_overlap.bed".format(conds[0], conds[1], ref))
    elif len(conds) == 3 :
        union_peaks_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}u{}_{}.optimal.Peak_overlap.bed".format(conds[0], conds[1], conds[2], ref))
    else :
       union_peaks_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_all_conds_{}.optimal.Peak_overlap.bed".format(ref))
    include: os.path.join(RULES, "union_peaks.rules")
    final_output.extend(expand(union_peaks_output, zip, CALLER=CALL_MOD, MARK=MARK_OK))

    #----------------------------------
    # get GFF from peak files
    #----------------------------------

    if len(conds) == 2 :
        bed_to_gff_input = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}_{}.optimal.Peak_overlap.bed".format(conds[0], conds[1], ref))
        bed_to_gff_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}_{}.optimal.Peak_overlap.gff".format(conds[0], conds[1], ref))
    elif len(conds) == 3 :
        bed_to_gff_input = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}u{}_{}.optimal.Peak_overlap.bed".format(conds[0], conds[1], conds[2],  ref))
        bed_to_gff_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}u{}_{}.optimal.Peak_overlap.gff".format(conds[0], conds[1], conds[2], ref))
    else :
        bed_to_gff_input = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_all_conds_{}.optimal.Peak_overlap.bed".format(ref))
        bed_to_gff_output = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_all_conds_{}.optimal.Peak_overlap.gff".format(ref))

    bed_to_gff_logs = os.path.join(analysis_dir, "09-CountMatrix/{CALLER}/logs/{MARK}_Optimal_Peaks_bed2gff.out")
    final_output.extend(expand(bed_to_gff_output, zip, CALLER=CALL_MOD, MARK=MARK_OK))
    include: os.path.join(RULES, "bed_to_gff.rules")


    def getBAMFilesByMark(wildcards):
        return expand(os.path.join(analysis_dir, "%s/{MARK}_{COND}_{REP}_%s_sort%s.bam" % (biasedRegions_dir, ref, biasedRegions)),  
            MARK=wildcards.MARK, COND=conds, REP=rep)
 

    #----------------------------------
    # feature Count on peaks
    #----------------------------------


    feature_counts_input = getBAMFilesByMark
    feature_counts_optional_input = []
    if config["differential_analysis"]["input_counting"]:
        feature_counts_optional_input += expand("{}/{{INPUT}}_{}_sort{}.bam".format(biasedRegions_dir, ref, biasedRegions), INPUT=CORR_INPUT)
    feature_counts_output_count = os.path.join(analysis_dir, "09-CountMatrix/{CALLER}/{MARK}_Matrix_Optimal_Peak.mtx")
    if len(conds) == 2 :
        feature_counts_gff = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}_{}.optimal.Peak_overlap.gff".format(conds[0], conds[1], ref))
    elif len(conds) == 3 :
        feature_counts_gff = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_{}u{}u{}_{}.optimal.Peak_overlap.gff".format(conds[0], conds[1], conds[2], ref))
    else :
        feature_counts_gff = os.path.join(analysis_dir, "09-CountMatrix/{{CALLER}}/{{MARK}}_all_conds_{}.optimal.Peak_overlap.gff".format(ref))

    feature_counts_log = os.path.join(analysis_dir, "09-CountMatrix/{CALLER}/logs/{MARK}_counts.out")
    feature_counts_options = "-t peak -g gene_id"
    feature_counts_threads = 4
    final_output.extend(expand(feature_counts_output_count, zip, CALLER=CALL_MOD, MARK=MARK_OK))
    include: os.path.join(RULES, "feature_counts.rules")

    #----------------------------------
    # differential analysis on peaks
    #----------------------------------
    

    method = config["differential_analysis"]["method"]
    norm = config["differential_analysis"]["normalisation"]
    if config["differential_analysis"]["batch"] :
        norm += "_batch"

    chipflowr_init_input = feature_counts_output_count
    chipflowr_init_conds = conds
    chipflowr_init_rep = rep
    chipflowr_init_method = method
    chipflowr_init_norm = norm
    if config["differential_analysis"]["spikes"] and config["design"]["spike"]:
        chipflowr_init_spikes = spikes_counting_output_json
        chipflowr_init_input_done = spikes_counting_output_json
    else:
        chipflowr_init_spikes = ""
        chipflowr_init_input_done = feature_counts_output_count
    chipflowr_init_padj = config["differential_analysis"]["pAdjustMethod"]
    chipflowr_init_alpha = config["differential_analysis"]["alpha"]
    chipflowr_init_batch = config["differential_analysis"]["batch"]
    chipflowr_init_output_dir = os.path.join(analysis_dir, "10-DifferentialAnalysis/{{CALLER}}/{{MARK}}_{}_{}".format(method, norm))
    chipflowr_init_config_r = os.path.join(analysis_dir, "10-DifferentialAnalysis/{{CALLER}}/{{MARK}}_{}_{}/config.R".format(method, norm))
    chipflowr_init_genome = ref
    include: os.path.join(RULES, "chipflowr_init.rules")


    chipflowr_config_r = chipflowr_init_config_r
    chipflowr_output_dir = os.path.join(analysis_dir, "10-DifferentialAnalysis/{{CALLER}}/{{MARK}}_{}_{}".format(method, norm))
    chipflowr_report = os.path.join(analysis_dir, "10-DifferentialAnalysis/{{CALLER}}/{{MARK}}_{}_{}/{{MARK}}_Stat_report_{}_{}.html".format(method, norm,method, norm))
    chipflowr_logs = os.path.join(analysis_dir, "10-DifferentialAnalysis/{{CALLER}}/{{MARK}}_{}_{}/{{MARK}}_{}_{}_Log.txt".format(method, norm, method, norm))
    final_output.extend(expand(chipflowr_report,  CALLER=CALL_MOD, MARK=MARK_OK))
    include: os.path.join(RULES, "chipflowr.rules")


#----------------------------------  
# IGV session
#----------------------------------

if config["igv_session"]["do"]:
    igv_session_input = expand(bamCoverage_output, SAMPLE=IP_ALL, REF=ref, allow_missing=True)
    if len(IP_REP) > 1 and config["macs2"]["do"]:
        if config["intersectionApproach"]["do"]:
            igv_session_input += expand(intersectionApproach_output, CALLER="macs2_" + model, IP_IDR=IP_REP)
        elif model in ["narrow"] and not config["intersectionApproach"]["do"] and config["compute_idr"]["do"]:
            igv_session_input += expand(select_peaks_output, CALLER="macs2", IP_IDR=IP_REP)
        else :
            igv_session_input += expand(macs2_output, SAMPLE=IP_ALL)
    elif len(IP_REP) > 1 and config["seacr"]["do"]:
        igv_session_input += expand(intersectionApproach_output, CALLER="seacr_" + config["seacr"]["threshold"], IP_IDR=IP_REP)
    if len(IP_ALL) > 1 and config["macs2"]["do"]:
        igv_session_input += expand(macs2_output, SAMPLE=IP_ALL)
    elif len(IP_ALL) > 1 and config["seacr"]["do"]:
        igv_session_input += expand(seacr_output, SAMPLE=IP_ALL)

    igv_session_output = os.path.join(analysis_dir, "12-IGV/igv_session.xml")
    igv_session_genome = ref
    igv_session_mark = marks
    igv_session_conds = conds
    igv_session_wdir = os.path.join(analysis_dir, "12-IGV/")
    igv_session_autoScale = config["igv_session"]["autoScale"]
    igv_session_normalize = config["igv_session"]["normalize"]
    igv_session_log_out = os.path.join(analysis_dir, "12-IGV/logs/igv_session.out")
    final_output.extend([igv_session_output])
    include: os.path.join(RULES, "igv_session.rules")


#----------------------------------  
# MultiQC report
#----------------------------------
try : model_dir
except NameError : model_dir = "seacr_" + config["seacr"]["threshold"]
if not config['seacr']['do'] and not config['macs2']['do']:
    model_dir = "multiqc"

multiqc_input = final_output
multiqc_input_dir = analysis_dir
multiqc_logs = os.path.join(analysis_dir, "11-Multiqc/multiqc.log")
multiqc_output = os.path.join(analysis_dir, config['multiqc']['output-directory'] + "/%s/multiqc_report.html" % (model_dir))
multiqc_options = config['multiqc']['options'] + " -c config/multiqc_config.yaml "
multiqc_output_dir = os.path.join(analysis_dir, config['multiqc']['output-directory'] + "/%s" % (model_dir))
final_output = [multiqc_output]
include: os.path.join(RULES, "multiqc.rules")

rule ePeak:
    input: final_output
        

#----------------------------------  
# Move needed files
#----------------------------------

onsuccess:
    import os
    # copy metrics json in the corresponding multiQC output when you are in exploratory mode
    import shutil
    import glob
    for file in glob.glob(os.path.join(analysis_dir, "*metrics.out")): 
        shutil.copy2(file, os.path.join(analysis_dir, config['multiqc']['output-directory'] + "/%s" % (model_dir)))

    # move cluster log files
    pattern = re.compile("slurm.*")
    dest = os.path.join(analysis_dir, "cluster_logs")
    for filepath in os.listdir("."):
        if pattern.match(filepath):
            if not os.path.exists(dest):
                os.makedirs(dest)
            shutil.move(filepath, dest)











