#########################################################################
# ePeak: Standardize and reproducible ChIP-seq analysis from raw        #
#           data to differential analysis                               #
# Authors: Rachel Legendre, Maelle Daunesse                             #
# Copyright (c) 2019-2020  Institut Pasteur (Paris) and CNRS.           #
#                                                                       #
# This file is part of ePeak workflow.                                  #
#                                                                       #
# ePeak is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# ePeak is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with ePeak (LICENSE).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



# ========================================================
# Config file for ePeak pipeline
#=========================================================

# directory where fastq are stored
input_dir: data
# How mate pair are written in fastq
input_mate: '_R[12]'
# filename extension
input_extension: '.fastq.gz'
# directory where you want 
analysis_dir: .
# tmpdir: write temporary file on this directory (default /tmp/, but could be "/local/scratch/")
tmpdir: "/tmp/"

#===============================================================================
# Design information. These informations will be used during the
# peak calling step
#
# :Parameters:
#
# - design_file: Path to a design file in tabulated format. See documentation
# - marks: list of different marks (comma-separated)
# - condition: list of different conditions (comma-separated). NB: cond1 must be the reference condition
# - replicates: How the replicate is writed in the files (REP or Rep or rep ?)
# - spike: set to 'yes' if you have spikes in your data. Possible values: {yes, no}
# - spike_genome_file: path to genome file used for spike-in
#===============================================================================

design:
    design_file: config/design.txt    
    marks: H3K27ac, Klf4
    condition: shCtrl, shUbc9
    replicates: Rep
    spike: no
    spike_genome_file: /path/to/genome/directory/dmel9.fa

#===============================================================================
# Indexing section: if checked, indexes for bowtie2 will be produced in 
# genome_directory
#
# :Parameters:
#
# - index: assuming needed indexes are here if no
# - genome_directory: directory where all indexed are written
# - name: name of prefix use in all output files from mapping
# - fasta_file: path to Reference Genome in fasta format
#===============================================================================

genome:
    index: yes
    genome_directory: genome/
    name: mm10
    fasta_file: genome/mm10.fa

#===============================================================================
# FastQC section
#
# :Parameters:
#
# - options: Any valid FastQC options
#===============================================================================

fastqc:
    options: ''
    threads: 4   

#===============================================================================
# Quality trimming and adapter removal with cutadapt
#
# :Parameters:
#
# - remove: skip this step if data are clean
# - adapter_list: a string or file (prefixed with *file:*)
# - m: 30 means discard trimmed reads that are shorter than 30.
# - mode: could be g for 5', a for 3', b for both 5'/3'
# - options: any options recognised by cutadapt. For paired-end reads,
#            specific options must be added here (except -p option, automatically
#            used in the rule for them)
# - quality: from 0 to 30, based en Phred-score quality
# - threads: number of threads 
#
#===============================================================================

adapters:
    remove: yes
    adapter_list: "ATAGATCTCGTCTAGCT"
    tool_choice: cutadapt 
    m: 25
    mode: g
    options: -O 6 --trim-n --max-n 1 -j 4
    quality: 30
    threads: 4



#===============================================================================
# bowtie2_mapping used to align reads against genome file
#
# :Parameters:
#
# - options: any options recognised by bowtie2 tool
# - threads: number of threads to be used
#===============================================================================


bowtie2_mapping:
#   options: "--dovetail --no-mixed --no-discordant " for paired-end data
    options: "--very-sensitive "
    threads: 4

#===============================================================================
# mark duplicates (picard-tools) allows to mark PCR duplicate in BAM files
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored.
# - dedup_IP: If false, only INPUT files will be deduplicated (ie duplicated reads 
#             are removed from output), IP files are only marked (ie duplicated
#             reads are writted with appropriate flags set. If true all files 
#             will be deduplicated. Default value: true. 
# - threads: number of threads to be used
#===============================================================================

mark_duplicates:
    do: yes
    dedup_IP: 'True' 
    threads: 4

#===============================================================================
# remove biased genomic regions
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored.
# - bed_file: path to BED file containing all biased regions
#===============================================================================


remove_biasedRegions:
    do: yes
    bed_file: genome/mm10.blacklist.bed
    threads: 1



#===============================================================================
# peak calling with macs2.
#
# :Parameters:
#
# - model: model used by MACS2. Could be 'narrow' or 'broad'.
# - no-model: use --no-model option. See MACS2 documentation. Possible values: {yes, no}
# - options: any option recognized by MACS2. For paired-end reads, it is very recommended
# to put --keep-dup 2500
# - cutoff: The q-value (minimum FDR) cutoff to call significant regions. 
# - genomeSize: It's the mappable genome size or effective genome size which is 
# defined as the genome size which can be sequenced. Because of the repetitive 
# features on the chromsomes, the actual mappable genome size will be smaller
# than the original size, about 90% or 70% of the genome size. Can be an integer 
# (1.0e+4 or 10000) or a shortcut, using MACS2 predefined shortcuts : hs (human, 
# 2.7e9 - recommended for hg18 genome assembly), mm (mouse, 1.87e9), ce (nematode, 9e7),
# dm (fruit fly, 1.2e8). Default : hs
# - readLength: in order to compute the best shift with PPQT when no_model is set
#
#===============================================================================


macs2:
    do: yes 
    mode_choice: 'narrow'    ## may be broad
    no_model: no             ## may be yes
    options: "--keep-dup all "
    cutoff: 0.1
    genomeSize: mm
    readLength: 50


#===============================================================================
# Peak calling with SEACR https://github.com/FredHutch/SEACR
# (recommended  for cut&run data)
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored.
# - threshold: could be 'stringent' of 'relaxed'
# - norm: specify "norm" for normalized or "non" for non-normalized data processing
#===============================================================================


seacr:
    do: no
    threshold: 'stringent'
    norm: 'norm'

#===============================================================================
# Compute IDR on replicates, pseudo-replicates and pooled replicates
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored.
# - rank: which column to use to rank peaks. Options: signal.value, p.value, q.value, columnIndex
# - thresh: report statistics for peaks with a global idr below this value. Default: 0.05
#
#===============================================================================

compute_idr:
    do: yes
    rank: 'signal.value'
    thresh: 0.05


#===============================================================================
# Compute intersection approach on replicates
#
# :Parameters:
#
# - do: if set to 'yes', will compute the intersection approach and use it
#   to select reproducible peaks. (for narrow only, correspond to the default broad approach)
# - ia_overlap: percentage of overlap between the peaks to be selected (-f parameter of bedtools intersect). Default: 0.8
#
#===============================================================================

intersectionApproach:
    do: no
    ia_overlap: 0.8

#===============================================================================
# Compute differential analysis
#
# :Parameters:
#
# - method: could be "Limma" or "DEseq2"
# - normalization: could be "geometrical", "spikes", "scale", "quantile", "cyclicloess"
# - spikes: True or False. If you have spikes, you can run ChIPuanaR with or without spikes normalization in order to compare.
# - pAdjustMethod: Limma and DEseq2 options. For Limma, "none", "BH", "BY" and "holm" are possible.
#                  For DESeq2, "holm" "hochberg" "hommel" "bonferroni" "BH" "BY" "fdr" and "none" are accepted.
# - alpha: 0.05 by default
# - batch: NULL or a vector with batch effects as c("","")
# - input_counting: add all input in count matrix
#===============================================================================

differential_analysis:
    do: yes
    method: "Limma" 
    normalisation: "quantile" 
    spikes: no
    pAdjustMethod: "BH"
    alpha: 0.05
    batch: NULL
    input_counting: yes


#############################################################################
# bamCoverage from Deeptools
#  see https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html
#
# :Parameters:
#
# - do: if 'no', this rule is ignored.
# - options: any parameter recognized by Deeptools (see Deeptools manual)
#
#===============================================================================

bamCoverage:
    do: yes
    options: "--binSize 10 --effectiveGenomeSize 2913022398 --normalizeUsing RPGC" 
    threads: 4

#############################################################################
# GeneBody heatmap plot from Deeptools
# see https://deeptools.readthedocs.io/en/develop/content/tools/plotHeatmap.html#usage-examples
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - regionsFileName: File name or names, in BED or GTF format, containing the regions to plot.
#
#===============================================================================

geneBody:
    do: yes
    regionsFileName: genome/mm10.refGene.gtf
    threads: 4

#==============================================================================
# IGV_session produce XML session readable by IGV browser with coverage and
# peak files
#
# :Parameters:
#
# - do: if unchecked, this rule is ignored
# - autoScale:
# - normalize:
#==============================================================================

igv_session:
    do: yes
    autoScale: True
    normalize: False


#===============================================================================
#   MultiQC aggregates results from bioinformatics analyses across many
#   samples into a single report.
#
# :Parameters:
#
# - options: any options recognised by multiqc
# - output-directory: Create report in the specified output directory
#===============================================================================


multiqc:
    options: " -f -e macs2 -x 03-Deduplication/*spikes* -x 02-Mapping/*_spike*"
    output-directory: "11-Multiqc"
