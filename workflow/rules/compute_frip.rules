#########################################################################
# ePeak: Standardize and reproducible ChIP-seq analysis from raw        #
#           data to differential analysis                               #
# Authors: Rachel Legendre, Maelle Daunesse                             #
# Copyright (c) 2019-2020  Institut Pasteur (Paris) and CNRS.           #
#                                                                       #
# This file is part of ePeak workflow.                                  #
#                                                                       #
# ePeak is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# ePeak is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with ePeak (LICENSE).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################


rule compute_frip:
    input:
        bam = compute_frip_bam,
        bed = compute_frip_peaks
    output:
        compute_frip_csv
    params:
        paired = compute_frip_paired
    singularity:
        "epeak.img"
    log:
        out = compute_frip_log
    shell:
        """
        temp_file=$(mktemp)

        if [[ {params.paired} == "True" ]]
        then
            #write reads as bedpe bed file to get fragments instead of reads
            bedtools bamtobed -bedpe -i {input.bam} > $temp_file 
        else
            bedtools bamtobed -i {input.bam} > $temp_file
        fi


        #compute number of fragments overlap peaks
        mapReads=$(bedtools intersect -a $temp_file -b {input.bed} -wa -u | wc -l)

        #compute number of mapped reads
        totReads=$(samtools view -c -F 260 {input.bam})

        #compute FRiP
        totReads/mapReads

        # plot_type: 'generalstats'
        Sample  FRiP
        H3K27ac_shCtrl  1.06
        H3K27ac_shUbc9  1.14
        Klf4_shCtrl 1.34
        Klf4_shUbc9 1.19
        """
