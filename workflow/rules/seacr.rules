#########################################################################
# ePeak: Standardize and reproducible ChIP-seq analysis from raw        #
#           data to differential analysis                               #
# Authors: Rachel Legendre, Maelle Daunesse                             #
# Copyright (c) 2019-2020  Institut Pasteur (Paris) and CNRS.           #
#                                                                       #
# This file is part of ePeak workflow.                                  #
#                                                                       #
# ePeak is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# ePeak is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with ePeak (LICENSE).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################



rule seacr:
    input:
        ip = seacr_bed_ip,
        input = seacr_bed_input
    output:
        seacr_output
    params:
        prefix = seacr_output_prefix,
        threshold = seacr_bed_threshold,
        norm = seacr_bed_norm 
    singularity:
        "epeak.img"
    log:
        out = seacr_logs_std,
        err = seacr_logs_err
    shell:
        """
        bash workflow/scripts/SEACR_1.3.sh {input.ip} {input.input} {params.norm} {params.threshold} {params.prefix} > {log.out} 2> {log.err}

        """
