#########################################################################
# ePeak: Standardize and reproducible ChIP-seq analysis from raw        #
#           data to differential analysis                               #
# Authors: Rachel Legendre, Maelle Daunesse                             #
# Copyright (c) 2019-2020  Institut Pasteur (Paris) and CNRS.           #
#                                                                       #
# This file is part of ePeak workflow.                                  #
#                                                                       #
# ePeak is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# ePeak is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with ePeak (LICENSE).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################


rule plotFingerprint:
    input:
        plotFingerprint_input
    output:
        plot = plotFingerprint_output,
        raw  = plotFingerprint_output_raw
    params:
        options = plotFingerprint_options
    log:
        out = plotFingerprint_logs
    singularity:
        "epeak.img"
    envmodules:
        "deepTools"
    threads:
        config['bamCoverage']['threads']
    shell:
        """
        tmp="{input}"
        infiles=($tmp)
        if [[ ${{#infiles[@]}} > 1 ]] ; then
            IP=${{infiles[0]}} ; IP=$(basename ${{IP%_sort*}})
            INPUT=${{infiles[1]}} ; INPUT=$(basename ${{INPUT%_sort*}})
            plotFingerprint --bam {input} --plotFile {output.plot} --outRawCounts {output.raw} --labels ${{IP}} ${{INPUT}} {params.options} --numberOfProcessors {threads} 2> {log.out}
        else 
            IP=${{infiles[0]}} ; IP=$(basename ${{IP%_sort*}})
            plotFingerprint --bam {input} --plotFile {output.plot} --outRawCounts {output.raw} --labels ${{IP}} {params.options} --numberOfProcessors {threads} 2> {log.out}
        fi
        """
